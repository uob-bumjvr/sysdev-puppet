class sysdev_core::profiles::centos_central_vm {
  # SysOps supplied centosbase
  include uobprofiles::centosbase
  # SysDev profiles below
  include sysdev_core::profiles::firewall
  include sysdev_core::profiles::mlocate
  include sysdev_core::profiles::winca
  
  # This fact is only set by the Vagrant provision script.
  # Any central provisioned VMs won't have it.
  if ($facts['uob_puppet_agent'] == 'central') {
    include sysdev_core::profiles::vagrant_user
  }
}


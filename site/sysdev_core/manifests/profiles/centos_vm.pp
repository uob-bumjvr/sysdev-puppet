class sysdev_core::profiles::centos_vm {
  if $facts['uob_puppet_agent'] == 'local' {
    include sysdev_core::profiles::centos_local_vm
  } else {
    include sysdev_core::profiles::centos_central_vm
  }
}
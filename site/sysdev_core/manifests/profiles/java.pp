# Install Jave 8, and removes all other Java versions.

class sysdev_core::profiles::java {

  package {'java-1.8.0-openjdk':
    ensure => present,
  }

  package {'java-1.8.0-openjdk-devel':
    ensure => present,
  }

  package {'java-1.7.0-openjdk-devel':
    ensure => absent,
  }

  package {'java-1.7.0-openjdk':
    ensure => absent,
  }

  package {'java-1.6.0-openjdk-devel':
    ensure => absent
  }

  package {'java-1.6.0-openjdk':
    ensure => absent
  }
}
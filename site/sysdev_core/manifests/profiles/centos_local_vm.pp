# A base profile used for local dev VMs
# This doesn't include things like authentication, networker, hyper-v agents etc
class sysdev_core::profiles::centos_local_vm {
  include sysdev_core::profiles::time
  include sysdev_core::profiles::yum
  include sysdev_core::profiles::firewall
  include sysdev_core::profiles::mlocate
  include sysdev_core::profiles::winca
}


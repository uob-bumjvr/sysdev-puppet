# Configures Apache as a reverse proxy to another HTTP server on localhost (generally Tomcat),
# and configures SSL.  This also opens the firewall ports 80 and 443.  If you don't supply the
# SSL details, a selfsigned certificate will be used.  By default it will proxy the root (/) 
# context, if you want another context supply the details in the $path param.
#
# @param $path hash The local and remote path to proxy, default = {local => '/', remote => '/'}
# @param $remote_port integer The remote port to proxy to, default = 8080.
# @param $ssl_key string the SSL key
# @param $ssl_cert string the SSL certificate
# @param $ssl_chain string the SSL chain
class sysdev_core::profiles::apache_ssl_reverse_proxy (
    $proxies = [ {'path' => '/', 'url' => 'http://localhost:8080/'} ],
    $ssl_key = "-----BEGIN RSA PRIVATE KEY-----\nMIIEowIBAAKCAQEAovjrMhThYoaFdM2c459p6YSGfUzmFtsTofRTBOQixCfCLuqu\nQeXD3Q/ZEVnO8etnYxDtMP8akHGkQTsa8AxN0RFFglMZzefcu0fr69lvsFubDBjd\nT+qapz+sLd3EUdCV6IWwC9Pbj1DRE0rSZRPt1eXcIko9tL8d7H2fpXOlJcCkrbas\nRsteMyjVZf0Swu5NNEWlbqzwW3vGw9DfM8pi7tsUv8W0WfVZjqeOh/hZga+fTl0m\nSS5XB54FmrtYpPyEmNCr1koHJNwwSGBQJTZbHDCe39MvtpHDX67STF/Tt8/EOJRv\npYlhPNrxyjNzYDnTITLrxqv4ydg4FQLfNLWtzwIDAQABAoIBACVaYiXnbUU0ye/c\nlKIDTrYsHtsnPfFtIxHSuxh65ZEJ/9LbDTejXwIS+NCmQe1m5BprrcG2JrOGqHwe\n/OyaFVfe2oylWVu9UfVG5cb33kaQqHfcVxVTE0FUMbWNh8qswT87LP94/nDZuCHw\n20o5iCCnf44IcB42U7h5jpbQa8C8CgxDnDCOu0vO2aVGAZNc5gb18M76uxw4pFs8\nAgq66IS0LWzCKHhyTj0k1dszHeuqie/ogjOT0D39D2vsE4QxIgDJSjLavNnSeH1y\n7Ke0fa3seX7xNBn4YLyKNT7YgxrDkssZnHgt8p9WFNb+mvBn2wusroShSZgv3ME/\nhwzRSSkCgYEA1N+LxiIRu2fL+GeNJrlehKDmP5tylox4+PYXhV/2OcMqk1rLKFsG\nmBHTtbUVbU+9e/mH/Pq3sMAyBf/6RewxWZSMV2odIuQo3gquP4hSKRWzBccXIvAh\n8mEYsQJ/m06zw4r57f9sOUmhNbd1ecGJkhxG8kTi678971W6eYv2JqUCgYEAw/1Q\nVZVhIpbVljaH8Bqy2dep6a/JaBdhAbfVuPKkm26n5YoAtmNV69kuZTjJKz15200E\n+0oZqsO+divu8eSQxquFIHTipCDO0rGqMHjgoH1g7MUK30SUWBgeFYuQHrjtJHk9\nu5S5Hj92ildMNylIunCwJq6g19e9ORDhAqbRDGMCgYAe48l+mNcF4MwCYdRjuCZ1\nDHwc4su/+OZdCijjm8c5Ho8BDv3H/113l66nAX34fMe8Q7tuBFu/dEROBpUkrF+E\n5j9vN+C+fWlIvzgF1BxIBfQSdI9w7HOgnmJc3UXzHXqMd5BI7vWfYoXYK6UKZAE4\niI3q/gAHwZqVLlV+6cZwWQKBgF/jT6qPqzaBFX6or1mJtndeTINIvgFvbsaoaqhS\nDrb9LKfhHPgfIaqCV3Cul73QFg51YDqRxn0Fx+mJFR7D9RE+WaPHjBtA8e+jfOC6\nQyxCK2mr/eW+Z9Wx9wYTgi+FyoBN0SY1XVyvMA//JgrW5/5Po99JEOiJZMrGt0/U\nzfKBAoGBAKgBuNwZRCbAPWYMS9GlQa/mOkNh/FjiQ667bWxhimaa0WYXMbfUSvpO\ncHWauQwmgvxQcG28DVGOxBPyju8pOiM8UZDNx2Mcf37Jt4fc5sodu9CLTE5Qpzb6\nevL6pj84iwHfePLlxboyNgxL6SrCg0nRGqNE97IELvrMiMzoewSZ\n-----END RSA PRIVATE KEY-----\n",
    $ssl_cert = "-----BEGIN CERTIFICATE-----\nMIIC9zCCAd+gAwIBAgIJAKASTV71ZuOeMA0GCSqGSIb3DQEBBQUAMBIxEDAOBgNV\nBAMMB2RldmNlcnQwHhcNMTYwOTA2MTIxOTM2WhcNMjYwOTA0MTIxOTM2WjASMRAw\nDgYDVQQDDAdkZXZjZXJ0MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA\novjrMhThYoaFdM2c459p6YSGfUzmFtsTofRTBOQixCfCLuquQeXD3Q/ZEVnO8etn\nYxDtMP8akHGkQTsa8AxN0RFFglMZzefcu0fr69lvsFubDBjdT+qapz+sLd3EUdCV\n6IWwC9Pbj1DRE0rSZRPt1eXcIko9tL8d7H2fpXOlJcCkrbasRsteMyjVZf0Swu5N\nNEWlbqzwW3vGw9DfM8pi7tsUv8W0WfVZjqeOh/hZga+fTl0mSS5XB54FmrtYpPyE\nmNCr1koHJNwwSGBQJTZbHDCe39MvtpHDX67STF/Tt8/EOJRvpYlhPNrxyjNzYDnT\nITLrxqv4ydg4FQLfNLWtzwIDAQABo1AwTjAdBgNVHQ4EFgQUhDXf0bSJTVE8EqTS\nf7Uo7/EIGwQwHwYDVR0jBBgwFoAUhDXf0bSJTVE8EqTSf7Uo7/EIGwQwDAYDVR0T\nBAUwAwEB/zANBgkqhkiG9w0BAQUFAAOCAQEAXZQ2a2vNKwedMMfGG1vAOKK3DX3y\nFicdneF+kQq7HCBkX7UqNZSneZGaWciI446nue26osexSLMuL9nAUiTU5ar3w34M\n5MeYDqD13Wb8bZJ20l/3CCLOCbUa+FDQZBJ+wv/wXVuwBv8XxvlIhzk73oH10vCv\n7EF2IW8miVjdwk71RHE1E5dXODl7alPpXMw9uIWKdnIXmFnStE58/Nd8LKHRdq7i\nXnxiQnAjXm7XnYvVwYZVg6KK4PZW3BnGDUNFbbO9KtHKO6n8Mnepuy1m9b8BXsxg\nPJUn0BFC6vxh8E0NODy7oCHFmRSZCs1WLWr2u4f8q0LQ+6XT2r8tVHBdKQ==\n-----END CERTIFICATE-----\n",
    $ssl_chain = ''
) {

  class {'apache':
    default_vhost => false,
  }

  # Needed so puppet knows where to copy the SSL keys to
  $conf_dir = '/etc/httpd/conf.d'

  file {"${conf_dir}/ssl.key":
    content => $ssl_key
  }
  file {"${conf_dir}/ssl.cert":
    content => $ssl_cert
  }
  file {"${conf_dir}/ssl.chain":
    content => $ssl_chain
  }


  apache::vhost { 'localhost':
    port            => '80',
    docroot         => '/var/www',
    redirect_status => 'permanent',
    redirect_dest   => "https://%{SERVER_NAME}/%${1}"
  }

  apache::vhost { 'localhost-ssl':
    port                => '443',
    docroot             => '/var/www',
    proxy_pass          => $proxies,
    proxy_preserve_host => true,
    request_headers     => [
      'set X-Forwarded-Proto "https"',
    ],
    ssl                 => true,
    ssl_cert            => "${conf_dir}/ssl.cert",
    ssl_key             => "${conf_dir}/ssl.key",
    ssl_chain           => "${conf_dir}/ssl.chain",
  }

  firewall { '100 allow http':
    proto  => 'tcp',
    dport  => 80,
    action => 'accept',
  }

  firewall { '101 allow https':
    proto  => 'tcp',
    dport  => 443,
    action => 'accept',
  }

}

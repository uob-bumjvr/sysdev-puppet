# Installs the Sonatype Nexus repository manager
class sysdev_core::profiles::nexus {

  $install_dir = '/opt/nexus'
  $install_version = '2.14.0-01'

  include sysdev_core::profiles::java

  # add user to run service
  user { 'nexus':
    ensure   => 'present',
    provider => useradd
  }

  notify { "downloading version ${install_version}": }

  $nexus_dir = "nexus-${install_version}"
  $download_filename = "nexus-${install_version}-bundle.tar.gz"
  $download_url = "http://download.sonatype.com/nexus/oss/${download_filename}"

  exec { "/usr/bin/mkdir -p ${install_dir}":
    creates => $install_dir
  }

  exec { "/usr/bin/curl ${download_url} -Lo ${install_dir}/${download_filename}":
      creates => "${install_dir}/${download_filename}"
  }

  exec { "/usr/bin/tar -xvzf ${install_dir}/${download_filename} -C ${install_dir}":
      creates => "${install_dir}/${nexus_dir}/NOTICE.txt",
  }

  # symlink to this version 
  file { "${install_dir}/nexus":
      ensure => link,
      target => "${install_dir}/${nexus_dir}",
  }
  # ensure nexus is owner of the symlink (service needs write access)
  exec { "/usr/bin/chown -hR nexus:nexus ${install_dir}": }

  notify { 'configuring nexus service': }

  # configures the nexus web address root to be "\"
  file { 'nexus-config':
      ensure => present,
      path   => "${install_dir}/nexus/conf/nexus.properties",
      source => 'puppet:///modules/sysdev_core/nexus/nexus.properties',
      owner  => 'nexus',
      group  => 'nexus'
  }

  notify { 'installing nexus service': }

  # we do this by copying over our unit (.service) file
  # unfortunately puppet doesn't allow you to register services so we have to use exec commands

  file { '/etc/systemd/system/nexus.service':
      ensure  => file,
      content => template('sysdev_core/nexus/nexus.service.erb'),
  }
  file { '/etc/systemd/system/nexus':
      ensure => 'link',
      target => '/etc/systemd/system/nexus.service',
  }

  # always reload the services, we need to use a name here to avoid conflicting with other modules
  exec { 'nexus service reload':
    command => '/usr/bin/systemctl daemon-reload'
  }

  # start the service
  service { 'nexus':
      ensure   => running,
      provider => 'systemd',
      enable   => true
  }

}

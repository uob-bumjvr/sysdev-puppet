# Installs the 'locate' program which provides an indexed
# file search.

class sysdev_core::profiles::mlocate {
  package { 'mlocate':
    ensure => present,
  }
  exec { 'create mlocate.db':
    command => '/usr/bin/updatedb',
    creates => '/var/lib/mlocate/mlocate.db',
    require => Package['mlocate'],
  }
}
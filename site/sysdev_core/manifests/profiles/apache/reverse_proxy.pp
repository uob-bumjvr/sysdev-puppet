# Configures Apache as a reverse proxy to another HTTP server on localhost (e.g. Tomcat). 
# By default it will proxy the root (/) context to localhost:8080, if you want 
# another context, or more proxies supply the $proxies argument below.
#
# NOTE: To use this profile, you must first have included the 'sysdev_core::profiles::apache' profile.
#
# @param $proxies Array The local and remote path to proxy, default = {path => '/', url => ''}
class sysdev_core::profiles::apache::reverse_proxy (
    $proxies = [ {'path' => '/', 'url' => 'http://localhost:8080/'} ],
) {

  $conf_dir = '/etc/httpd/conf.d'

  include apache::mod::proxy
  include apache::mod::headers

  if $facts['selinux'] {
    exec {'/usr/sbin/setsebool -P httpd_can_network_connect 1':}
  }

  apache::vhost { 'reverse-proxy-ssl':
    servername          => $facts['fqdn'],
    port                => '443',
    priority            => 10,  # must be lower than the default-ssl vhost, which is 25.
    docroot             => '/var/www',
    proxy_pass          => $proxies,
    proxy_preserve_host => true,
    request_headers     => [
      'set X-Forwarded-Proto "https"',
    ],
    ssl                 => true,
    ssl_cert            => "${conf_dir}/ssl.cert",
    ssl_key             => "${conf_dir}/ssl.key",
    ssl_chain           => "${conf_dir}/ssl.chain",
    access_log          => false,  # disable configuring the logs, so output just goes into default Apache logs
    error_log           => false,
  }

}

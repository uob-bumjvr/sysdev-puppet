# Configures Apache to use CAS for authn and LDAP group membership for authz.
# This configues authentication based on URL rather than adding the configuration
# to the Puppet vhost.
#
# NOTE: The cas_authn profile MUST already be configured.
#
# @params $path The path to authenticate
class sysdev_core::profiles::apache::authz_ldap (
  $path = '/',
  $url = 'ldaps://ldap-ads.services.bris.ac.uk:3269/DC=ads,DC=bris,DC=ac,DC=uk?sAMAccountName?sub?(objectClass=*)',
  $group = 'CN=ISYS Systems Development Team,OU=Security Groups (Andrew Dixon),OU=ISYS,DC=ads,DC=bris,DC=ac,DC=uk',
  $bind_dn = 'it-sysdev-webmon@ads.bris.ac.uk',
  $bind_password = ''
) {

  include apache::mod::authnz_ldap

$conf = @('ENDDOC')
<Location $path>
    Authtype CAS

    CASScope            $path
    AuthLDAPUrl         $url
    AuthLDAPBindDN      $bind_dn
    AuthLDAPBindPassword $bind_password

    # What depth of nested groups to search before giving up
    AuthLDAPMaxSubGroupDepth 5

    # Defines which values of the LDAP attribute "objectClass" specify a group member to be another group, rather than a user
    AuthLDAPSubGroupClass group

    <RequireAny>
      # The group to require membership of.  
      # The exact full entry to put below can be found from the distinguishedName attribute of the group in AD
      Require ldap-group $group
    </RequireAny>
</Location>
|ENDDOC

  file {'/etc/httpd/conf.d/ldap_authz.conf':
    ensure  => present,
    content => $conf,
    notify  => Service['httpd']
  }

}
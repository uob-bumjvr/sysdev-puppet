# Installs and configures mod_auth_cas.  This won't automatically 'casify' 
# any pages, you need to do that separately in your vhost setting with the
# 'auth_type' setting.  To use CAS with LDAP groups for authorisation, see
# 'authz_ldap' file.
#
# @depend stahnma-epel
#
# @param $cas_server The cas server to use
# @param $cas_proxied_as The service FQDN to use, if this Apache server is behind a reverse proxy (e.g. an F5 VIP)
class sysdev_core::profiles::apache::authn_cas (
  $cas_server = 'sso-ai.bris.ac.uk/sso-mock',
  $cas_proxied_as = undef
) {

  include epel; # mod_auth_cas is in epel

  class {'apache::mod::auth_cas':
    cas_certificate_path   => '/etc/pki/tls/certs/ca-bundle.crt',
    cas_login_url          => "https://${cas_server}/login",
    cas_validate_url       => "https://${cas_server}/serviceValidate",
    cas_proxy_validate_url => "https://${cas_server}/proxyValidate",
    cas_root_proxied_as    => $cas_proxied_as,
  }

}
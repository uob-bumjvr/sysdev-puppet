class sysdev_core::profiles::grafana() {

  $grafana_version = '3.0.4-1464167696'

  yumrepo {'grafana':
    baseurl  => 'https://packagecloud.io/grafana/stable/el/6/$basearch',
    name     => 'grafana',
    descr    => 'grafana',
    gpgcheck => '1',
    gpgkey   => 'https://packagecloud.io/gpg.key https://grafanarel.s3.amazonaws.com/RPM-GPG-KEY-grafana',
  }
  package {'grafana':
    ensure  => $grafana_version,
    require => Yumrepo['grafana']
  }
  exec {'/usr/bin/yum versionlock grafana':}

  # For some reason we need to do this manually on this package
  # http://docs.grafana.org/installation/rpm/
  exec {'grafana reload':
    command => '/usr/bin/systemctl daemon-reload',
    require => Package['grafana']
  }

  service {'grafana-server':
    ensure => 'running',
    enable => true
  }
}
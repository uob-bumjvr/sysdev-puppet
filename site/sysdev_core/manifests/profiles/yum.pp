# Installs the local UoB yum remo, the EPEL repo, and by default configures yum to autoupdate
#
# @depend aco-yum_autoupdate
# @depend stahnma-epel
#
# @param $auto_update boolean whether to enable auto update of packages, default = true
class sysdev_core::profiles::yum(
#  $auto_update = true
) {
# Commenting out for now, because there is a conflict between "yum" and "yum-cron" package
# and yum auto-updates are something that it's unclear whos remit (sysdev vs sysops) they
# are in.
#  if $auto_update {
#    class { 'yum_autoupdate':
#      service_ensure => running,
#    }
#  } else {
#    class { 'yum_autoupdate':
#      service_ensure => stopped,
#      service_enable => false,
#    }
#  }

  yumrepo{'local':
    baseurl  => 'http://packages.bris.ac.uk/centos/$releasever/sysops',
    enabled  => true,
    gpgcheck => 0, # this has to be '0' - setting it to false causes puppet to ignore the value.
    protect  => false,
    priority => 1,
  }

  # Needed because if you install a specific package version using Puppet, it 
  # still autoupdates to a newer version.  We need to install this plugin, and 
  # then manually enforce Yum to  not autoupdate the package version where needed with:
  #    exec {'/usr/bin/yum versionlock <package>':}
  package {'yum-plugin-versionlock':
    ensure => 'present',
  }

  # Where a package is available in different repos then cab specify the priority when
  # adding the repo.  This is needed when e.g. there is a more recent package version in 
  # a vendors repo than a more general one like epel.
  package {'yum-plugin-priorities':
    ensure => 'present',
  }

  include epel
}
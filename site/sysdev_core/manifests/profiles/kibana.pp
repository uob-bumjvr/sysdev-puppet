class sysdev_core::profiles::kibana() {

  $kibana_version = '4.5.1-1'

  yumrepo {'kibana':
    baseurl  => 'http://packages.elastic.co/kibana/4.5/centos',
    name     => 'kibana4',
    descr    => 'kibana4',
    gpgcheck => '1',
    gpgkey   => 'https://packages.elastic.co/GPG-KEY-elasticsearch',
  }
  package {'kibana':
    ensure  => $kibana_version,
    require => Yumrepo['kibana4']
  }
  exec {'/usr/bin/yum versionlock kibana':}

  file {'/opt/kibana/config/kibana.yml':
    content => template('sysdev_core/kibana/kibana.yml.erb'),
    notify  => Service['kibana'],
  }

  # Install TimeLion
  exec {'/opt/kibana/bin/kibana plugin -i elastic/timelion':
    creates => '/opt/kibana/installedPlugins/timelion'
  }
  # Kibana minimizes and optimizes its resources, so needs to be able to write here
  file {'/opt/kibana/optimize/':
    ensure  => 'directory',
    owner   => 'kibana',
    group   => 'kibana',
    recurse => true,
  }

  service {'kibana':
    ensure => 'running',
    enable => true
  }
}
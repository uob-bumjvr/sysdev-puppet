# Configures NTP servers, and sets timezone.
#
# @param $servers string[] Array of NTP servers to use
#
# @depend puppetlabs/ntp --version 4.2.0
# @depend saz/timezone
class sysdev_core::profiles::time (
  $servers = ['ntp1.bris.ac.uk', 'ntp2.bris.ac.uk', 'ntp3.ja.net']
) {

  class { 'ntp':
    servers  => $servers,
    restrict => ['127.0.0.1'],
  }

  class {'timezone':
    timezone => 'Europe/London',
  }

}


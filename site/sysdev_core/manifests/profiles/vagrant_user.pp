class sysdev_core::profiles::vagrant_user {
  
  exec {'add vagrant user to sshd allow':
    command => "/bin/sed  -i  '/^AllowUsers vagrant/! s/AllowUsers/AllowUsers vagrant/' /etc/ssh/sshd_config"
  } -> exec {'/usr/bin/systemctl restart sshd':}
  
  
  File{'/etc/sudoers.d/vagrant':
    content => 'vagrant = (ALL) NOPASSWD: ALL'
  }
  
}
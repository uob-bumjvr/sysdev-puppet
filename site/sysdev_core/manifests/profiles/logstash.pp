class sysdev_core::profiles::logstash (
  $es_ips = ['localhost'],
  $ls_certificate = '',
  $tcp_ports = ['5556']
) {
  include firewall

  yumrepo {'logstash2':
    baseurl  => 'https://packages.elastic.co/logstash/2.3/centos',
    name     => 'logstash2',
    descr    => 'logstash2',
    gpgcheck => '1',
    gpgkey   => 'https://packages.elastic.co/GPG-KEY-elasticsearch',
  }

  package {'logstash':
    ensure => $ls_version
  }

  exec {'/usr/bin/yum versionlock logstash':}

  file {'/etc/logstash/' :
    ensure  => 'directory',
    source  => 'puppet:///modules/sysdev_core/logstash',
    recurse => true
  }

  # needed for the exec subscribe below
  file {'/etc/logstash/filebeat.template.json' :
    ensure  => 'directory',
    source  => 'puppet:///modules/sysdev_core/logstash/filebeat.template.json',
    recurse => true
  }

  # needed for the exec subscribe below
  file {'/etc/logstash/sysdev-filebeat.template.json' :
    ensure  => 'directory',
    source  => 'puppet:///modules/sysdev_core/logstash/sysdev-filebeat.template.json',
    recurse => true
  }

  file {'/etc/logstash/conf.d/logstash.conf':
    require => Package['logstash'],
    content => template('sysdev_core/logstash//logstash.conf.erb'),
    notify  => Service['logstash'],
  }

  # Load the "filebeat" Elasticsearch index template
  # https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-template.html
  exec{'/usr/bin/curl -XPUT http://localhost:9200/_template/filebeat -d@/etc/logstash/filebeat.template.json':
    subscribe   => File['/etc/logstash/filebeat.template.json'],
    refreshonly => true,
  }
  exec{'/usr/bin/curl -XPUT http://localhost:9200/_template/sysdev-filebeat -d@/etc/logstash/sysdev-filebeat.template.json':
    subscribe   => File['/etc/logstash/sysdev-filebeat.template.json'],
    refreshonly => true,
  }

  service {'logstash':
    ensure => 'running',
    enable => true
  }

  $tcp_ports.each |$index, $port| {
    firewall { "20${index} allow logstash on ${port}":
      proto  => 'tcp',
      dport  => $port,
      action => 'accept',
    }
  }
}

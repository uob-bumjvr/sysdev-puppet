class sysdev_core::profiles::apache_mod_auth_cas () {
  class {'apache':}
  class {'apache::mod::authnz_ldap':}
  class {'apache::mod::proxy':}
  class {'apache::mod::headers':}

  include winca;

  include epel;
  package {'mod_auth_cas':
    ensure => 'present'
  }

  exec {'/usr/sbin/setsebool -P httpd_can_network_connect 1':}

  file {'/var/cache/mod_auth_cas':
    ensure => 'directory',
    owner  => 'apache',
    group  => 'apache'
  }


  file {'/etc/httpd/conf.d/elk.conf':
    ensure  => 'present',
    content => template('elk/httpd-elk.conf.erb'),
    notify  => Service['httpd'],
  }

}
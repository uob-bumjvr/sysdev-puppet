class sysdev_core::profiles::firewall {

  # We're currently using the SysOps firewall settings.  

  # If we decide to manage the firewall ourselves from scratch, check out
  # this file at commit 2f4f38d to see the setup according to Puppets 
  # best practice.

  # However we do want Purge to be on, so set it here
  resources { 'firewall':
    purge => true,
  }



}

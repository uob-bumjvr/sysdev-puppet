# Installs NodeJS 5 from the "nodesource" repo
#
# @depends puppet-nodejs
#
# @param $version The version of node to install, must be one that is valid according to the docs 
#                  (https://github.com/voxpupuli/puppet-nodejs/#repo_url_suffix)
class sysdev_core::profiles::node (
  $version = '5.x'
) {
  class { 'nodejs':
    repo_url_suffix => $version,
    repo_priority   => '50' # To avoid problems with EPEL repo see https://github.com/voxpupuli/puppet-nodejs#repo_priority
  }
}
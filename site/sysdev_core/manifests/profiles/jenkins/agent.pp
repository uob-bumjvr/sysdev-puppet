class sysdev_core::profiles::jenkins::agent {

  include sysdev_core::profiles::java
  include sysdev_core::profiles::node

  # Install Maven directly from Apache.  We need a more recent version that is in the CentOS repos,
  # and ironically there doesn't appear to be a yum repo we can get it from.
  exec {'/usr/bin/curl http://mirrors.ukfast.co.uk/sites/ftp.apache.org/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.tar.gz -o /opt/maven-3.3.9.tar.gz' :
    creates => '/opt/maven-3.3.9.tar.gz'
  }
  exec {'/usr/bin/tar -xzvf /opt/maven-3.3.9.tar.gz -C /opt':
    creates => '/opt/apache-maven-3.3.9'
  }

  # Node Stuff
  package { 'grunt':
    ensure   => 'present',
    provider => 'npm'
  }

  package { 'gulp':
    ensure   => 'present',
    provider => 'npm'
  }

  package { 'swagger-cli':
    ensure   => 'present',
    provider => 'npm'
  }

}
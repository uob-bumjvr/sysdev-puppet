# Installs Jenkins 2 Master
class sysdev_core::profiles::jenkins::master {

  include sysdev_core::profiles::java

  yumrepo {'jenkins':
    baseurl  => 'http://pkg.jenkins.io/redhat-stable',
    name     => 'jenkins',
    descr    => 'jenkins',
    gpgcheck => '1',
    gpgkey   => 'http://pkg.jenkins.io/redhat-stable/jenkins.io.key',
  }

  package {'jenkins':
    require => Yumrepo['jenkins'],
  }

  service {'jenkins':
    ensure => 'running',
    enable => true
  }

  file {'/etc/sysconfig/jenkins' :
    ensure => 'present',
    source => 'puppet:///modules/sysdev_core/jenkins/sysconfig',
    owner  => 'root',
    group  => 'root',
    notify => Service['jenkins']
  }

  firewall { '100 allow jenkins ssh on 2222':
    proto  => 'tcp',
    dport  => 2222,
    action => 'accept',
  }

  firewall { '110 allow jenkins on 8080':
    proto  => 'tcp',
    dport  => 8080,
    action => 'accept',
  }

  package {'git':
    ensure => 'present'
  }

}
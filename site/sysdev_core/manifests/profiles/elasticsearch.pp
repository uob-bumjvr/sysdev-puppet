class sysdev_core::profiles::elasticsearch (
    $es_ips = ['localhost'],
    $es_cluster_name='elasticsearch',
    $es_data_dir = '/elasticsearch-data',
    $es_heap_size= '1024m',
    $es_version = '2.3.4-1'
    ) {

    include sysdev_core::profiles::java;

    $es_ips.each |$ip| {
      firewall { "allow from ${ip}":
        proto  => 'tcp',
        source => $ip,
        action => 'accept'
      }
    }

  yumrepo {'elasticsearch2':
    baseurl  => 'https://packages.elastic.co/elasticsearch/2.x/centos',
    name     => 'elasticsearch2',
    descr    => 'elasticsearch2',
    gpgcheck => '1',
    gpgkey   => 'https://packages.elastic.co/GPG-KEY-elasticsearch',
  }

  package {'elasticsearch':
    ensure  => $es_version,
    require => Yumrepo['elasticsearch2']
  }

  exec {'/usr/bin/yum versionlock elasticsearch':}

  file {$es_data_dir:
    ensure  => directory,
    require => Package['elasticsearch'],
    owner   => 'elasticsearch',
    group   => 'elasticsearch',
  }

  file {'/etc/elasticsearch/logging.yml' :
    ensure  => 'directory',
    source  => 'puppet:///modules/sysdev_core/elasticsearch/logging.yml',
    recurse => true,
    owner   => 'root',
    group   => 'elasticsearch'
  }
  file {'/etc/elasticsearch/elasticsearch.yml':
    require => Package['elasticsearch'],
    content => template('sysdev_core/elasticsearch/elasticsearch.yml.erb'),
    owner   => 'root',
    group   => 'elasticsearch',
    notify  => Service['elasticsearch'],
  }

  file {'/etc/sysconfig/elasticsearch':
    require => Package['elasticsearch'],
    content => template('sysdev_core/elasticsearch/sysconfig.erb'),
    notify  => Service['elasticsearch'],
    owner   => 'root',
    group   => 'elasticsearch',
  }

  # Edit the ES systemd file to enable mem locking by uncommenting the line "#LimitMEMLOCK" line
  # See https://www.elastic.co/guide/en/elasticsearch/reference/current/setup-configuration.html#setup-configuration-memory
  exec{'/usr/bin/sed -i "s/# LimitMEMLOCK=infinity/LimitMEMLOCK=infinity/"  /usr/lib/systemd/system/elasticsearch.service; /usr/bin/systemctl daemon-reload':}

  service {'elasticsearch':
    ensure => 'running',
    enable => true
  }

  # Installs  http://www.elastichq.org/
  # Available on <hostname>/_plugin/hq/
  # e.g. http://sysdev-logging-test.services.bris.ac.uk/elasticsearch/_plugin/hq/
  exec {'/usr/share/elasticsearch/bin/plugin install royrusso/elasticsearch-HQ':
    creates => '/usr/share/elasticsearch/plugins/hq'
  }

  yumrepo {'elastic-curator':
    baseurl  => 'http://packages.elastic.co/curator/4/centos/7',
    name     => 'elastic-curator',
    descr    => 'elastic-curator',
    gpgcheck => '1',
    gpgkey   => 'https://packages.elastic.co/GPG-KEY-elasticsearch',
  }

  package {'python-elasticsearch-curator':
    require => Yumrepo['elastic-curator'],
  }
}

class sysdev_core::roles::jenkins_master {
  include sysdev_core::profiles::centos_vm
  include sysdev_core::profiles::jenkins::agent
  include sysdev_core::profiles::jenkins::master
}
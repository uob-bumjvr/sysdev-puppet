class sysdev_core::roles::nexus {
  include sysdev_core::profiles::centos_vm
  include sysdev_core::profiles::apache
  
  class{'sysdev_core::profiles::apache::reverse_proxy':
    proxies => [
      {'path' => '/', url => 'http://localhost:8081/'},
    ]
  }
  
  include sysdev_core::profiles::nexus
}
class sysdev_core::roles::elk {
  
  include sysdev_core::profiles::centos_vm
  
  include sysdev_core::profiles::apache
  # include sysdev_core::profiles::apache::authn_cas
  # include sysdev_core::profiles::apache::authz_ldap
  
  
  class{'sysdev_core::profiles::apache::reverse_proxy':
    proxies => [
      {'path' => '/elasticsearch', url => 'http://localhost:9200'},
      {'path' => '/kibana', url => 'http://localhost:5601'},
      {'path' => '/grafana', url => 'http://localhost:3000'}
    ]
  }
  
  
  include sysdev_core::profiles::elasticsearch
  include sysdev_core::profiles::logstash
  include sysdev_core::profiles::kibana
  include sysdev_core::profiles::grafana

}
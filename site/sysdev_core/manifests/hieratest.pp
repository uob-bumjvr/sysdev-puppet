class sysdev_core::hieratest (
  $top_name = 'empty',
  $role_name = 'empty',
  $service_level_name = 'empty'
) {

  notify {"top_name ${top_name}":}
  notify {"role_name ${role_name}":}
  notify {"service_level ${service_level_name}":}

}
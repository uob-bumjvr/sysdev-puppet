# Sysdev Roles & Profiles

This repo contains:

  - Puppetfile for r10k dependency management
  - Configuration data in the form of hiera.
  - Roles and profiles Puppet classes

Currently this is only used to build SysDev "infrastructure" nodes, which are nodes which
run internal SysDev tools such as Jenkins, Nexus, ELK etc.  That is why this is on the 
"sysdev_infrastructure" branch/environment.

All nodes (prod and test) are then in this one environment.  The configuration differences between nodes is determined using
hiera configuration.

There is only one actual module, which is in `site/sysdev_core`.   This contains all
the role and profile classes, and follows the [Puppet best practice](https://docs.puppet.com/pe/2016.2/r_n_p_intro.html)
so each node should have one and only one role applied to it.

## Usage

To apply an existing role (e.g. "jenkins master") and user environment (e.g. "test") to a new set of nodes:
 
  - In the PE web console, create a new classification group named `<role> (<environment>)`, e.g. `jenkins master (test)`
  - Under "rules" pin the nodes that will be used
  - Under "classes" apply the `sysdev_code::roles::jenkins_master` class
  - Under "variables" create 2 variables:
  
    - `role` = the name of the role class, e.g. "jenkins_master"
    - `service_level` = the user environment e.g. "test"

In this repository, all the hiera configuration is in "config".  This has a hierarchy that uses the roles and environment variables above as:
  
  - `config/roles/<rolename>.yaml` = Configuration that is applied to all nodes that have this role.
  - `config/roles/<rolename>/test.yaml` = Configuration to override above with specific data for each user environment.
  
In general, there shouldn't need to be too much configuration because your profiles should contain sane defaults that will apply
to most situations.  Only override values here if you have some specific need to - don't just put everything into hiera.


## Development

This repo also contains a Vagrantfile that will bring up a VM suitable for developing the Puppet classes.  It will run the code direct from 
this repo, and so provides a very simple and easy way to develop and test code, and then push it in to the central puppet master for deployment.  
You can quickly spin up a local VM, and use Puppet to apply a role (e.g. Jenkins) to it.

  > Always develop on a branch - never commit direct to master!


## Running and developing locally with vagrant

Requirements:
 
  - Vagrant 1.8.5
  - VirtualBox 5
  - cygwin or at least "ssh.exe" on your path

Once on your branch you may provision two types of machine using vagrant:

1. `local` - creates a standalone VM with puppet tools installed, but doesn't join a puppet master.  This is suitable for most development
2. `central` - creates a VM and joins it to Puppet Enterprise 4 test server [sisko](https://sisko.isys.bris.ac.uk).  This will need you to have admin access to Siko to authorise the client joining.

### Steps for `local`

1. Run `vagrant up` and then `vagrant ssh` to get into the VM.
2. Run `sudo su` to become root, and then `cd /vagrant`
3. To install PuppetFile dependencies run `./install`.   The first time it will prompt you to add a private key that can be used to access gitlab.    If you haven't set SSH keys in gitlab, you can use the same ones as bitbucket, see similar instructions as for BitBucket at https://wikis.bris.ac.uk/display/itsysdev/Git+-+Setting+Up). 
4. Then `./run <class>` will run that class.  e.g. `./run roles::elk` will run the `manifests/roles/elk.pp` class which will apply all the configuration for an ELK stack.
5. You can then commit and push the code to your branch as you go like normal.

Once you are reasonably confident your code is correct and it's pushed to the remote branch, you can create a Puppet environment for the feature, and pull some
central test VMs into this environment.  Once it is fully tested on these nodes, you can then merge the branch to "sysdev_infrastructure" which will
deploy the change to all prod nodes.
TODO: more detail on the above.


### Steps for `central`

This uses a test Puppet Enterprise server called `sisko.isys`, which has the control repo `git@gitlab.isys.bris.ac.uk:pe-r10k/kid.git`, and 
it will join your vagrant VM to this PE server.

This will provide a more realistic test environment for the code, as it will also apply all the base configuration from SysOps (e.g. firewall rules etc).  This isn't required for most changes, normally just using the `local` Vagrant VM will be enough.  This profile can be used for developing and testing changes that require using a PE4 server.

1. Run `vagrant up central` and then `vagrant ssh central` to get into the VM, and note your hostname.
2. Run `sudo su` to become root.
3. Go to https://sisko.isys.bris.ac.uk/ in order to authorise the vagrant VM (admin user & password in Keepass) 
4. Assign a role class and create variables as described above "Usage" above.
4. Run `puppet agent --noop -t` this should result in nothing happening unless you assign roles in https://sisko.isys.bris.ac.uk/ to the vagrant machine name. remove the --noop argument to actually perform the action.

Notes on using `central`:

 - It will fail to decrypt the bind_pw for `nslcd`, so you will always receive an error about `nslcd` failing to start.
 - If you can't see the vagrant VM on https://sisko.isys.bris.ac.uk/ you may need to re-run `vagrant up central` after a `vagrant destroy`

## To reset your development environment

1. On your host machine run `vagrant destroy`
2. Run `vagrant\reset.bat` to delete temporary files.

## Connecting to the vagrant guest vm

You may wish to check web sites using your host browser.

1. Run `ifconfig` on the vagrant box
2. One of the ip addresses listed should work, try the one starting 172 first if it exists.
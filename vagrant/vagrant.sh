yum install -y git
yum install -y rubygems
yum install -y dos2unix
yum install -y deltarpm
yum install -y nano

# Install Puppet Agent from UoB "sisko" server if we are on the non-Puppet base box.
if ! [[ -d /etc/puppetlabs ]]; then
  # This generates a random but English hostname e.g. "gray-moon.vagrant" and requests
  # to join the "sisko.isys" Puppet Master
  gem install haikunator
  hostname $(ruby -e "require 'haikunator'; print Haikunator.haikunate(0)").vagrant

  curl -k https://sisko.isys.bris.ac.uk:8140/packages/current/install.bash | sudo bash
  
  # Stop the puppet daemon, so it doesn't apply changes automatically - use 'puppet agent -t'
  # so can run it manually and see the output.
  systemctl stop puppet
  systemctl disable puppet
  echo "export PATH=$PATH:/usr/local/bin/" > /etc/profile.d/setprofile.sh
  echo "export FACTER_UOB_PUPPET_AGENT=central" >> /etc/profile.d/setprofile.sh
  echo "environment = sysdev_infrastructure" >> /etc/puppetlabs/puppet/puppet.conf
else
  echo "export FACTER_UOB_PUPPET_AGENT=local" >> /etc/profile.d/setprofile.sh
  cp /vagrant/vagrant/hiera.yaml /etc/puppetlabs/code/hiera.yaml

  # We need the latest (not-released) version of librarian-puppet so it supports ":commit" 
  # references in the Puppetfile.  This means we can use exactly the same
  # Puppetfile here, and in the r10k repo

  gem install specific_install
  gem specific_install -l https://github.com/voxpupuli/librarian
  gem specific_install -l https://github.com/voxpupuli/librarian-puppet

  dos2unix -q /vagrant/run
  dos2unix -q /vagrant/install

  chmod u+x /vagrant/run
  chmod u+x /vagrant/install

fi

 